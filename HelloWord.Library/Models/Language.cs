﻿using HelloWord.Library.Models.Base;
using Newtonsoft.Json;

namespace HelloWord.Library.Models
{
    /// <summary>
    /// Language model
    /// </summary>
    public class Language : BaseModel
    {
        #region Attributes

        private string _locale;
        private string _name;

        #endregion

        #region Properties

        /// <summary>
        /// Language's locale code
        /// </summary>
        [JsonProperty("locale")]
        public string Locale
        {
            get => _locale;
            set
            {
                if (Equals(_locale, value))
                {
                    return;
                }

                _locale = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Language's name
        /// </summary>
        [JsonProperty("name")]
        public string Name
        {
            get => _name;
            set
            {
                if (Equals(_name, value))
                {
                    return;
                }

                _name = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public Language() { }

        /// <summary>
        /// Constructor with a locale and a name
        /// </summary>
        /// <param name="locale">Language's locale</param>
        /// <param name="name">Language's name</param>
        public Language(string locale, string name)
        {
            Locale = locale;
            Name = name;
        }

        /// <summary>
        /// Constructor with an id, a locale and a name
        /// </summary>
        /// <param name="id">Language's id</param>
        /// <param name="locale">Language's locale</param>
        /// <param name="name">Language's name</param>
        public Language(int id, string locale, string name) 
            : this(locale, name)
        {
            Id = id;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Description of a language item
        /// </summary>
        /// <returns>The string representation of the language</returns>
        public override string ToString()
        {
            return string.Format($"Language(Id: {Id}, Locale: {Locale}, Name: {Name})");
        }

        #endregion
    }
}
