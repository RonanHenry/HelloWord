﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.Globalization;
using HelloWord.Library.Enums;
using HelloWord.Library.Models.Base;
using Newtonsoft.Json;

namespace HelloWord.Library.Models
{
    /// <summary>
    /// Player model
    /// </summary>
    public class Player : BaseModel
    {
        #region Attributes

        private string _name;
        private Color _color;
        private Difficulty _difficulty;
        private Language _nativeLanguage;
        private Language _fluentLanguage;
        private ObservableCollection<Game> _games;
        private ObservableCollection<Stat> _stats;

        #endregion

        #region Properties

        /// <summary>
        /// Player's name
        /// </summary>
        [JsonProperty("name")]
        public string Name
        {
            get => _name;
            set
            {
                if (Equals(_name, value))
                {
                    return;
                }

                _name = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Player's color
        /// </summary>
        [JsonProperty("color")]
        public Color Color
        {
            get => _color;
            set
            {
                if (Equals(_color, value))
                {
                    return;
                }

                _color = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Player's selected difficulty
        /// </summary>
        [JsonProperty("difficulty")]
        public Difficulty Difficulty
        {
            get => _difficulty;
            set
            {
                if (Equals(_difficulty, value))
                {
                    return;
                }

                _difficulty = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Player's native language
        /// </summary>
        [JsonProperty("native_language")]
        public Language NativeLanguage
        {
            get => _nativeLanguage;
            set
            {
                if (Equals(_nativeLanguage, value))
                {
                    return;
                }

                _nativeLanguage = value;
                NativeLanguageId = NativeLanguage.Id;
                ApplicationLanguages.PrimaryLanguageOverride = NativeLanguage.Locale;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Player's native language id
        /// </summary>
        [JsonProperty("native_language_id")]
        public int NativeLanguageId { get; set; }

        /// <summary>
        /// Player's fluent language
        /// </summary>
        [JsonProperty("fluent_language")]
        public Language FluentLanguage
        {
            get => _fluentLanguage;
            set
            {
                if (Equals(_fluentLanguage, value))
                {
                    return;
                }

                _fluentLanguage = value;
                FluentLanguageId = FluentLanguage.Id;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Player's fluent language id
        /// </summary>
        [JsonProperty("fluent_language_id")]
        public int FluentLanguageId { get; set; }

        /// <summary>
        /// Player's list of games
        /// </summary>
        [JsonProperty("games")]
        public ObservableCollection<Game> Games
        {
            get => _games;
            set
            {
                if (Equals(_games, value))
                {
                    return;
                }

                _games = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Player's list of stats
        /// </summary>
        [JsonProperty("stats")]
        public ObservableCollection<Stat> Stats
        {
            get => _stats;
            set
            {
                if (Equals(_stats, value))
                {
                    return;
                }

                _stats = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public Player()
        {
            Games = new ObservableCollection<Game>();
            Stats = new ObservableCollection<Stat>();
        }

        /// <summary>
        /// Constructor with a name
        /// </summary>
        /// <param name="name">Player's name</param>
        public Player(string name) : this()
        {
            Name = name;
        }

        /// <summary>
        /// Constructor with a name, a native language and a fluent language
        /// </summary>
        /// <param name="name">Player's name</param>
        /// <param name="difficulty">Player's selected difficulty</param>
        /// <param name="nativeLanguage">Player's native language</param>
        /// <param name="fluentLanguage">Player's fluent language</param>
        public Player(string name, Difficulty difficulty, Language nativeLanguage, Language fluentLanguage) 
            : this(name)
        {
            Difficulty = difficulty;
            NativeLanguage = nativeLanguage;
            FluentLanguage = fluentLanguage;
        }

        /// <summary>
        /// Constructor with a name, a native language, a fluent language,
        /// a list of games and a list of stats
        /// </summary>
        /// <param name="name">Player's name</param>
        /// <param name="difficulty">Player's selected difficulty</param>
        /// <param name="nativeLanguage">Player's native language</param>
        /// <param name="fluentLanguage">Player's fluent language</param>
        /// <param name="games">Player's games</param>
        /// <param name="stats">Player's stats</param>
        public Player(string name, Difficulty difficulty, Language nativeLanguage, Language fluentLanguage, List<Game> games, List<Stat> stats) 
            : this(name, difficulty, nativeLanguage, fluentLanguage)
        {
            Games = new ObservableCollection<Game>(games);
            Stats = new ObservableCollection<Stat>(stats);
        }

        /// <summary>
        /// Constructor with an id, a name, a color, a native language, a fluent language,
        /// a list of games and a list of stats
        /// </summary>
        /// <param name="id">Player's id</param>
        /// <param name="name">Player's name</param>
        /// <param name="difficulty">Player's selected difficulty</param>
        /// <param name="color">Player's color</param>
        /// <param name="nativeLanguage">Player's native language</param>
        /// <param name="fluentLanguage">Player's fluent language</param>
        /// <param name="games">Player's games</param>
        /// <param name="stats">Player's stats</param>
        public Player(int id, string name, Difficulty difficulty, Color color, Language nativeLanguage, Language fluentLanguage, 
            List<Game> games, List<Stat> stats) 
            : this(name, difficulty, nativeLanguage, fluentLanguage, games, stats)
        {
            Id = id;
            Color = color;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Description of a player item
        /// </summary>
        /// <returns>The string representation of the player</returns>
        public override string ToString()
        {
            return string.Format(
                $"Player(Id: {Id}, " +
                $"Name: {Name}, " +
                $"Color: {Color}, " +
                $"Difficulty: {Difficulty}, " +
                $"NativeLanguage: {NativeLanguage}, " +
                $"FluentLanguage: {FluentLanguage}, " +
                $"Games: {string.Join(" / ", Games.Select(g => g.ToString()).ToArray())}, " +
                $"Stats: {string.Join(" / ", Stats.Select(s => s.ToString()).ToArray())})");
        }

        #endregion
    }
}
