﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using HelloWord.Library.Enums;
using HelloWord.Library.Models.Base;
using Newtonsoft.Json;

namespace HelloWord.Library.Models
{
    /// <summary>
    /// Game model
    /// </summary>
    public class Game : BaseModel
    {
        #region Attributes

        private Language _language;
        private Difficulty _difficulty;
        private ObservableCollection<Player> _players;
        private ObservableCollection<Stat> _stats;

        #endregion

        #region Properties

        /// <summary>
        /// Game's language
        /// </summary>
        [JsonProperty("languages")]
        public Language Language
        {
            get => _language;
            set
            {
                if (Equals(_language, value))
                {
                    return;
                }

                _language = value;
                LanguageId = _language.Id;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Game's language id
        /// </summary>
        [JsonProperty("language_id")]
        public int LanguageId { get; set; }

        /// <summary>
        /// Game's difficulty
        /// </summary>
        [JsonProperty("difficulty")]
        public Difficulty Difficulty
        {
            get => _difficulty;
            set
            {
                if (Equals(_difficulty, value))
                {
                    return;
                }

                _difficulty = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Game's list of players
        /// </summary>
        [JsonProperty("players")]
        public ObservableCollection<Player> Players
        {
            get => _players;
            set
            {
                if (Equals(_players, value))
                {
                    return;
                }

                _players = value;

                foreach (var player in _players)
                {
                    PlayerIds.Add(player.Id);
                }

                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Game players ids
        /// </summary>
        [JsonProperty("player_ids")]
        public List<int> PlayerIds { get; set; }

        /// <summary>
        /// Game's list of stats
        /// </summary>
        [JsonProperty("stats")]
        public ObservableCollection<Stat> Stats
        {
            get => _stats;
            set
            {
                if (Equals(_stats, value))
                {
                    return;
                }

                _stats = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public Game()
        {
            Players = new ObservableCollection<Player>();
            Stats = new ObservableCollection<Stat>();
            PlayerIds = new List<int>();
        }

        /// <summary>
        /// Constructor a language and a difficulty
        /// </summary>
        /// <param name="language">Game's language</param>
        /// <param name="difficulty">Game's difficulty</param>
        public Game(Language language, Difficulty difficulty) : this()
        {
            Language = language;
            Difficulty = difficulty;
        }

        /// <summary>
        /// Constructor with a language, a difficulty and a list of players
        /// </summary>
        /// <param name="language">Game's language</param>
        /// <param name="difficulty">Game's difficulty</param>
        /// <param name="players">List of players in the game</param>
        public Game(Language language, Difficulty difficulty, List<Player> players) 
            : this(language, difficulty)
        {
            Players = new ObservableCollection<Player>(players);
        }

        /// <summary>
        /// Constructor with an id, a language, a difficulty, a list of players and a list of stats
        /// </summary>
        /// <param name="id">Game's id</param>
        /// <param name="language">Game's language</param>
        /// <param name="difficulty">Game's difficulty</param>
        /// <param name="players">List of players in the game</param>
        /// <param name="stats">List of stats of the game</param>
        public Game(int id, Language language, Difficulty difficulty, List<Player> players, List<Stat> stats) 
            : this(language, difficulty, players)
        {
            Id = id;
            Stats = new ObservableCollection<Stat>(stats);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Description of a game item
        /// </summary>
        /// <returns>The string representation of the game</returns>
        public override string ToString()
        {
            return string.Format(
                $"Game(Id: {Id}, " +
                $"Language: {Language}, " +
                $"Difficulty: {Difficulty}, " +
                $"Players: {string.Join(" / ", Players.Select(p => p.ToString()).ToArray())}, " +
                $"Stats: {string.Join(" / ", Stats.Select(s => s.ToString()).ToArray())})");
        }

        #endregion
    }
}
