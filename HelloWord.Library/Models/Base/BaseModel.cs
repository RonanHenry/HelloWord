﻿using HelloWord.Library.Utils;

namespace HelloWord.Library.Models.Base
{
    /// <summary>
    /// Base model class containing the id field and the
    /// property changed event handler
    /// </summary>
    public class BaseModel : PropertyChangedWatcher
    {
        #region Attributes

        private int _id;

        #endregion

        #region Properties

        /// <summary>
        /// Object's id
        /// </summary>
        public int Id
        {
            get => _id;
            set
            {
                if (Equals(_id, value))
                {
                    return;
                }

                _id = value;
                OnPropertyChanged();
            }
        }

        #endregion
    }
}
