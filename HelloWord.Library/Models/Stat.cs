﻿using HelloWord.Library.Models.Base;
using Newtonsoft.Json;

namespace HelloWord.Library.Models
{
    /// <summary>
    /// Stat model
    /// </summary>
    public class Stat : BaseModel
    {
        private int _score;
        private Player _player;
        private Game _game;

        #region Properties

        /// <summary>
        /// Stat's score
        /// </summary>
        [JsonProperty("score")]
        public int Score
        {
            get => _score;
            set
            {
                if (Equals(_score, value))
                {
                    return;
                }

                _score = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Stat's player
        /// </summary>
        [JsonProperty("player")]
        public Player Player
        {
            get => _player;
            set
            {
                if (Equals(_player, value))
                {
                    return;
                }

                _player = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Stat's game
        /// </summary>
        [JsonProperty("game")]
        public Game Game
        {
            get => _game;
            set
            {
                if (Equals(_game, value))
                {
                    return;
                }

                _game = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public Stat()
        {
            Score = 0;
        }

        /// <summary>
        /// Constructor with a player and a game
        /// </summary>
        /// <param name="player">Stat's player</param>
        /// <param name="game">Stat's game</param>
        public Stat(Player player, Game game) : this()
        {
            Player = player;
            Game = game;
        }

        /// <summary>
        /// Constructor with an id, a score, a player and a game
        /// </summary>
        /// <param name="id">Stat's id</param>
        /// <param name="score">Stat's score</param>
        /// <param name="player">Stat's player</param>
        /// <param name="game">Stat's game</param>
        public Stat(int id, int score, Player player, Game game) 
            : this(player, game)
        {
            Id = id;
            Score = score;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Description of a stat item
        /// </summary>
        /// <returns>The string representation of the stat</returns>
        public override string ToString()
        {
            return string.Format($"Stat(Id: {Id}, Score: {Score}, Player: {Player}, Game: {Game})");
        }

        #endregion
    }
}
