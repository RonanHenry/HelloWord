﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HelloWord.Library.Services
{
    /// <summary>
    /// Http client service
    /// </summary>
    public class HttpClientService : HttpClient
    {
        #region Properties

        /// <summary>
        /// AWS server url to connect to the Rails web API
        /// </summary>
        public static readonly string BaseUrl = "http://18.195.149.39";

        /// <summary>
        /// Http client service instance
        /// </summary>
        public static readonly HttpClientService Instance = new HttpClientService();

        #endregion

        #region Contructors

        /// <summary>
        /// Constructor
        /// </summary>
        private HttpClientService()
        {
            Timeout = TimeSpan.FromSeconds(15);
            MaxResponseContentBufferSize = 256000;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get list of items of type T from the database
        /// </summary>
        /// <typeparam name="T">Type of items to retrieve</typeparam>
        /// <param name="url">API endpoint url</param>
        /// <returns>The list of T items</returns>
        public async Task<List<T>> GetAllAsync<T>(string url)
        {
            var uri = new Uri(string.Format($"{BaseUrl}{url}"));
            var response = await GetAsync(uri);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(response.ReasonPhrase);
            }

            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<T>>(content);
        }

        /// <summary>
        /// Get a single item of type T from the database
        /// </summary>
        /// <typeparam name="T">Type of the item to retrieve</typeparam>
        /// <param name="url">API endpoint url</param>
        /// <returns>The requested item</returns>
        public async Task<T> GetSingleAsync<T>(string url)
        {
            var uri = new Uri(string.Format($"{BaseUrl}{url}"));
            var response = await GetAsync(uri);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(response.ReasonPhrase);
            }

            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(content);
        }

        /// <summary>
        /// Save a new item in the database
        /// </summary>
        /// <typeparam name="T">Type of the item to save</typeparam>
        /// <param name="url">API endpoint url</param>
        /// <param name="item">Item to save</param>
        /// <returns>The inserted item</returns>
        public async Task<T> CreateAsync<T>(string url, T item)
        {
            var uri = new Uri(string.Format($"{BaseUrl}{url}"));
            var json = JsonConvert.SerializeObject(item);
            var request = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await PostAsync(uri, request);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(response.ReasonPhrase);
            }

            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(content);
        }

        /// <summary>
        /// Update an item in the database
        /// </summary>
        /// <typeparam name="T">Type of the item to update</typeparam>
        /// <param name="url">API endpoint url</param>
        /// <param name="itemData">Item's data to update</param>
        /// <returns>The updated item</returns>
        public async Task<T> UpdateAsync<T>(string url, T itemData)
        {
            var uri = new Uri(string.Format($"{BaseUrl}{url}"));
            var json = JsonConvert.SerializeObject(itemData);
            var request = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await PutAsync(uri, request);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(response.ReasonPhrase);
            }

            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(content);
        }

        /// <summary>
        /// Delete an item from the database
        /// </summary>
        /// <param name="url">API endpoint url</param>
        /// <returns>The task of the deletion</returns>
        public async Task DestroyAsync(string url)
        {
            var uri = new Uri(string.Format($"{BaseUrl}{url}"));
            var response = await DeleteAsync(uri);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(response.ReasonPhrase);
            }
        }

        #endregion
    }
}
