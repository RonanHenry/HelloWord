﻿using System;
using Windows.ApplicationModel.Core;
using Windows.Globalization;
using Windows.Media.SpeechRecognition;
using Windows.UI.Popups;
using HelloWord.Library.Events;

namespace HelloWord.Library.Services
{
    /// <summary>
    /// Speech to text service
    /// </summary>
    public class SpeechToTextService
    {
        #region Attributes

        private SpeechRecognizer _speechRecognizer;
        private SpeechToTextEventArgs _speechToTextEventArgs;
        private static SpeechToTextService _instance;

        #endregion

        #region Properties

        public static SpeechToTextService Instance => _instance ?? (_instance = new SpeechToTextService());

        #endregion

        #region Events

        public event EventHandler Start;

        public event SpeechToTextEventHandler Stop;

        public delegate void OnStopEventHandler(object sender, SpeechToTextEventArgs e);

        public event SpeechToTextEventHandler HasResult;

        public delegate void SpeechToTextEventHandler(object sender, SpeechToTextEventArgs e);

        #endregion
        
        #region Constructors

        private SpeechToTextService()
        {
            _speechToTextEventArgs = new SpeechToTextEventArgs();
        }
        
        #endregion

        #region Methods

        public async void StartRecognization(Language language, string[] responses)
        {
            OnStartEvent(new EventArgs());

            // Create an instance of SpeechRecognizer.
            _speechRecognizer = InitSpeechRecognizer(language);

            // Add a list constraint to the recognizer.
            var listConstraint = new SpeechRecognitionListConstraint(responses, "responses");
            _speechRecognizer.Constraints.Add(listConstraint);

            // Compile the constraint.
            await _speechRecognizer.CompileConstraintsAsync();

            _speechRecognizer.ContinuousRecognitionSession.ResultGenerated += ContinuousRecognitionSession_ResultGenerated;
            _speechRecognizer.ContinuousRecognitionSession.Completed += ContinuousRecognitionSession_Completed;

            if (_speechRecognizer.State == SpeechRecognizerState.Idle)
            {
                await _speechRecognizer.ContinuousRecognitionSession.StartAsync();
            }
        }

        public async void StopRecognization()
        {
            if (_speechRecognizer.State != SpeechRecognizerState.Idle)
            {
                await _speechRecognizer.ContinuousRecognitionSession.CancelAsync();
            }

            OnStopEvent(_speechToTextEventArgs);

            _speechToTextEventArgs = new SpeechToTextEventArgs();
        }

        private SpeechRecognizer InitSpeechRecognizer(Language language)
        {
            var speechRecognizer = new SpeechRecognizer(language);

            return speechRecognizer;
        }

        private void ContinuousRecognitionSession_ResultGenerated(
            SpeechContinuousRecognitionSession sender, SpeechContinuousRecognitionResultGeneratedEventArgs args)
        {
            if (args.Result.Confidence != SpeechRecognitionConfidence.Medium &&
                args.Result.Confidence != SpeechRecognitionConfidence.High)
            {
                return;
            }

            _speechToTextEventArgs.SpeechResult = args.Result.Text;
            OnHaveResultEvent(_speechToTextEventArgs);
        }

        private async void ContinuousRecognitionSession_Completed(
            SpeechContinuousRecognitionSession sender, SpeechContinuousRecognitionCompletedEventArgs args)
        {
            var dialog = new MessageDialog("Voice recognization time out and stop");

            switch (args.Status)
            {
                case SpeechRecognitionResultStatus.Success:
                    return;
                case SpeechRecognitionResultStatus.TimeoutExceeded:
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
                        Windows.UI.Core.CoreDispatcherPriority.Normal,
                        async () => { await dialog.ShowAsync(); });
                    break;
                default:
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
                        Windows.UI.Core.CoreDispatcherPriority.Normal,
                        async () =>
                        {
                            if (_speechRecognizer.State == SpeechRecognizerState.Idle)
                            {
                                await _speechRecognizer.ContinuousRecognitionSession.StartAsync();
                            }
                        });
                    break;
            }
        }

        protected virtual void OnStartEvent(EventArgs e)
        {
            var handler = Start;

            handler?.Invoke(this, e);
        }

        protected virtual void OnStopEvent(SpeechToTextEventArgs e)
        {
            var handler = Stop;

            handler?.Invoke(this, e);
        }

        protected virtual void OnHaveResultEvent(SpeechToTextEventArgs e)
        {
            var handler = HasResult;

            handler?.Invoke(this, e);
        }

        #endregion
    }
}
