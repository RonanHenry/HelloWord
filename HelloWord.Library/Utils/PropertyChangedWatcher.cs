﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using HelloWord.Library.Annotations;

namespace HelloWord.Library.Utils
{
    /// <summary>
    /// Property changed implementation
    /// </summary>
    public class PropertyChangedWatcher : INotifyPropertyChanged
    {
        #region Events

        /// <summary>
        /// Event fired when a property changes to update the view
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Methods

        /// <summary>
        /// Method called to fire the property changed event
        /// </summary>
        /// <param name="propertyName">Name of the property that has changed</param>
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
