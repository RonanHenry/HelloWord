﻿namespace HelloWord.Library.Enums
{
    /// <summary>
    /// Player colors
    /// </summary>
    public enum Color
    {
        /// <summary>
        /// Red color
        /// </summary>
        Red = 1,

        /// <summary>
        /// Green color
        /// </summary>
        Green,

        /// <summary>
        /// Blue color
        /// </summary>
        Blue,

        /// <summary>
        /// Yellow color
        /// </summary>
        Yellow
    }
}
