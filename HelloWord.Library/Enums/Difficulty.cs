﻿namespace HelloWord.Library.Enums
{
    /// <summary>
    /// Game difficulties
    /// </summary>
    public enum Difficulty
    {
        /// <summary>
        /// Easy difficulty
        /// </summary>
        Easy = 1,

        /// <summary>
        /// Medium difficulty
        /// </summary>
        Medium,

        /// <summary>
        /// Hard difficulty
        /// </summary>
        Hard
    }
}
