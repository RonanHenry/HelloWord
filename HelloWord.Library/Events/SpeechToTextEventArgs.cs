﻿using System;
using System.Text;

namespace HelloWord.Library.Events
{
    public class SpeechToTextEventArgs : EventArgs
    {
        public StringBuilder SpeechResultAll { get; } = new StringBuilder();

        public string SpeechResult
        {
            get
            {
                var lines = SpeechResultAll.ToString().Split('\n');
                return lines[lines.Length - 2];
            }

            set => SpeechResultAll.AppendLine(value);
        }
    }
}
