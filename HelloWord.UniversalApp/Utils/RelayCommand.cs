﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace HelloWord.UniversalApp.Utils
{
    /// <summary>
    /// Generic relay command implementation
    /// </summary>
    /// <typeparam name="T">Type of the command's arguments</typeparam>
    public class RelayCommand<T> : ICommand
    {
        #region Attributes

        private readonly Predicate<T> _canExecute;
        private readonly Action<T> _execute;

        #endregion

        #region Events

        /// <summary>
        /// Event fired to test if a command can be executed
        /// </summary>
        public event EventHandler CanExecuteChanged;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor for a command without a "can execute" check
        /// </summary>
        /// <param name="execute">Action to execute</param>
        public RelayCommand(Action<T> execute) : this(execute, null) { }

        /// <summary>
        /// Constructor for a command with a "can execute" check
        /// </summary>
        /// <param name="execute">Action to execute</param>
        /// <param name="canExecute">Predicate to decide if the action can be executed</param>
        public RelayCommand(Action<T> execute, Predicate<T> canExecute)
        {
            _execute = execute ?? throw new ArgumentException("execute");
            _canExecute = canExecute;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Predicate to decide if the action can be executed
        /// </summary>
        /// <param name="parameter">Action parameter</param>
        /// <returns>True if the action can be executed, false otherwise</returns>
        [DebuggerStepThrough]
        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke((T) parameter) ?? true;
        }

        /// <summary>
        /// Action to execute
        /// </summary>
        /// <param name="parameter">Action parameter</param>
        public void Execute(object parameter)
        {
            _execute((T) parameter);
        }

        /// <summary>
        /// Method called to manually evaluate the predicate
        /// </summary>
        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        #endregion
    }
}
