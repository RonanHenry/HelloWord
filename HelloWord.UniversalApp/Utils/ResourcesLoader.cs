﻿using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Resources;

namespace HelloWord.UniversalApp.Utils
{
    /// <summary>
    /// Resources loader
    /// </summary>
    public class ResourcesLoader : CustomXamlResourceLoader
    {
        #region Attributes

        private readonly ResourceLoader _loader;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public ResourcesLoader()
        {
            _loader = ResourceLoader.GetForViewIndependentUse();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get a resource value by it's id
        /// </summary>
        /// <param name="resourceId">Resource's id</param>
        /// <param name="objectType">Object type</param>
        /// <param name="propertyName">Property name</param>
        /// <param name="propertyType">Property type</param>
        /// <returns>The resource value</returns>
        protected override object GetResource(string resourceId, string objectType, string propertyName, string propertyType)
        {
            return _loader.GetString(resourceId);
        }

        #endregion
    }
}
