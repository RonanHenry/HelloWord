﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;

namespace HelloWord.UniversalApp.Services
{
    /// <summary>
    /// Navigation service
    /// </summary>
    public class NavigationService
    {
        #region Attributes

        /// <summary>
        /// List of the app's pages
        /// </summary>
        private readonly Dictionary<string, Type> _pages = new Dictionary<string, Type>();

        #endregion

        #region Properties

        /// <summary>
        /// Navigation service instance initialization
        /// </summary>
        private static readonly Lazy<NavigationService> NavigationInstance = new Lazy<NavigationService>(() => new NavigationService(), true);

        /// <summary>
        /// Navigation instance
        /// </summary>
        public static NavigationService Instance => NavigationInstance.Value;

        /// <summary>
        /// Main frame of the application managing all the navigation
        /// </summary>
        public Frame NavigationFrame { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Register a new page to the service
        /// </summary>
        /// <param name="pageName"></param>
        private void Configure(string pageName)
        {
            if (string.IsNullOrEmpty(pageName))
            {
                throw new ArgumentException("The page name cannot be empty");
            }

            if (_pages.ContainsKey(pageName))
            {
                return;
            }

            try
            {
                var currentAssembly = GetType().GetTypeInfo().Assembly;
                var name = string.Format($"{currentAssembly.FullName.Split(',')[0]}.Pages.{pageName}");
                var pageType = currentAssembly.GetType(name);

                _pages.Add(pageName, pageType);
            }
            catch (Exception)
            {
                throw new ArgumentException(string.Format($"Unable to map {pageName} to an actual type"), nameof(pageName));
            }
        }

        /// <summary>
        /// Navigate to a page with a transition
        /// </summary>
        /// <param name="pageName">Page's name</param>
        /// <param name="transition">Transition to use</param>
        public void NavigateTo(string pageName, NavigationTransitionInfo transition = null)
        {
            NavigateTo(pageName, null, transition);
        }

        /// <summary>
        /// Navigate to a page with a parameter and a transition
        /// </summary>
        /// <param name="pageName">Page's name</param>
        /// <param name="parameter">Parameter passed</param>
        /// <param name="transition">Transition to use</param>
        public void NavigateTo(string pageName, object parameter, NavigationTransitionInfo transition = null)
        {
            if (transition == null)
            {
                transition = new ContinuumNavigationTransitionInfo();
            }

            var locked = false;
            pageName += "Page";

            try
            {
                Monitor.Enter(_pages, ref locked);
                Configure(pageName);
                NavigationFrame.Navigate(_pages[pageName], parameter, transition);
            }
            finally
            {
                if (locked)
                {
                    Monitor.Exit(_pages);
                }
            }
        }

        /// <summary>
        /// Navigate to the previous page in history
        /// </summary>
        /// <param name="transition">Transition to use</param>
        public void GoBack(NavigationTransitionInfo transition = null)
        {
            if (!NavigationFrame.CanGoBack)
            {
                return;
            }

            if (transition != null)
            {
                NavigationFrame.GoBack(transition);
            }
            else
            {
                NavigationFrame.GoBack();
            }
        }

        /// <summary>
        /// Navigate to the next page
        /// </summary>
        public void GoForward()
        {
            if (NavigationFrame.CanGoForward)
            {
                NavigationFrame.GoForward();
            }
        }

        #endregion
    }
}
