﻿using Windows.UI.Xaml.Controls;
using HelloWord.UniversalApp.ViewModels;

namespace HelloWord.UniversalApp
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            DataContext = new MainViewModel(NavigationFrame);
        }
    }
}
