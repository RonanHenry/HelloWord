﻿using System;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using HelloWord.Library.Models;
using HelloWord.Library.Utils;

namespace HelloWord.UniversalApp.ViewModels.Base
{
    /// <summary>
    /// Base view model
    /// </summary>
    public class BaseViewModel : PropertyChangedWatcher
    {
        #region MyRegion

        private string _title;
        private Brush _headerBackground;
        private string _backImage;
        private Brush _titleForeground;
        private Player _player;

        #endregion

        #region Properties

        /// <summary>
        /// Page's title
        /// </summary>
        public string Title
        {
            get => _title;
            set
            {
                if (Equals(_title, value))
                {
                    return;
                }

                _title = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Page's header background
        /// </summary>
        public Brush HeaderBackground
        {
            get => _headerBackground;
            set
            {
                if (Equals(_headerBackground, value))
                {
                    return;
                }

                _headerBackground = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Back image to use in the header
        /// </summary>
        public string BackImage
        {
            get => _backImage;
            set
            {
                if (Equals(_backImage, value))
                {
                    return;
                }

                _backImage = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Foreground color of the title
        /// </summary>
        public Brush TitleForeground
        {
            get => _titleForeground;
            set
            {
                if (Equals(_titleForeground, value))
                {
                    return;
                }

                _titleForeground = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Current signed in player
        /// </summary>
        public Player Player
        {
            get => _player;
            set
            {
                if (Equals(_player, value))
                {
                    return;
                }

                _player = value;
                OnPropertyChanged();
                OnPlayerChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Resource loader
        /// </summary>
        public ResourceLoader ResourceLoader { get; set; }

        #endregion

        #region Events

        /// <summary>
        /// Player changed event
        /// </summary>
        public event EventHandler PlayerChanged;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseViewModel()
        {
            HeaderBackground = (Brush) Application.Current.Resources["LightBlueBrush"];
            BackImage = "back-arrow-light";
            TitleForeground = (Brush) Application.Current.Resources["WhiteBrush"];

            ResourceLoader = new ResourceLoader();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Player changed event handler method
        /// </summary>
        /// <param name="e"></param>
        public virtual void OnPlayerChanged(EventArgs e)
        {
            var handler = PlayerChanged;

            handler?.Invoke(this, e);
        }

        #endregion
    }
}
