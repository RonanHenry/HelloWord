﻿using System;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using HelloWord.Library.Enums;
using HelloWord.Library.Services;
using HelloWord.UniversalApp.Events;
using HelloWord.UniversalApp.Utils;
using HelloWord.UniversalApp.ViewModels.Base;
using HelloWord.UniversalApp.ViewModels.Controls;

namespace HelloWord.UniversalApp.ViewModels
{
    /// <summary>
    /// Settings view model
    /// </summary>
    public class SettingsViewModel : BaseViewModel
    {
        #region Properties

        /// <summary>
        /// Languages view model
        /// </summary>
        public LanguagesViewModel LanguagesViewModel { get; set; }

        #endregion

        #region Commands

        /// <summary>
        /// Difficulty checked command
        /// </summary>
        public ICommand DifficultyChecked { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public SettingsViewModel()
        {
            HeaderBackground = (Brush) Application.Current.Resources["LightGreyBrush"];
            BackImage = "back-arrow-dark";
            TitleForeground = (Brush) Application.Current.Resources["DarkGreyBrush"];
            Title = ResourceLoader.GetString("SettingsText");

            LanguagesViewModel = new LanguagesViewModel
            {
                FlagBorderColor = (Brush) Application.Current.Resources["LightGreyBrush"],
                FlagForegroundColor = (Brush) Application.Current.Resources["DarkGreyBrush"]
            };

            DifficultyChecked = new RelayCommand<string>(DifficultyCheckedExecute);

            PlayerChanged += (sender, args) =>
            {
                if (Player.NativeLanguage != null)
                {
                    LanguagesViewModel.NativeLanguage = Player.NativeLanguage;
                }

                if (Player.FluentLanguage != null)
                {
                    LanguagesViewModel.FluentLanguage = Player.FluentLanguage;
                }
            };

            LanguagesViewModel.LanguageChanged += OnLanguageChanged;
        }

        #endregion

        #region Methods

        private async void DifficultyCheckedExecute(string difficulty)
        {
            switch (difficulty)
            {
                case "Easy":
                    Player.Difficulty = Difficulty.Easy;
                    break;
                case "Hard":
                    Player.Difficulty = Difficulty.Hard;
                    break;
                default:
                    Player.Difficulty = Difficulty.Medium;
                    break;
            }

            await HttpClientService.Instance.UpdateAsync(string.Format($"/api/players/{Player.Id}"), Player);
        }

        private async void OnLanguageChanged(object sender, EventArgs e)
        {
            var eventArgs = (LanguageEventArgs) e;

            if (eventArgs.Type == "native")
            {
                Player.NativeLanguage = LanguagesViewModel.NativeLanguage;
            }
            else
            {
                Player.FluentLanguage = LanguagesViewModel.FluentLanguage;
            }

            await HttpClientService.Instance.UpdateAsync(string.Format($"/api/players/{Player.Id}"), Player);
        }

        #endregion
    }
}
