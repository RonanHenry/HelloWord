﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml.Controls;
using HelloWord.Library.Enums;
using HelloWord.Library.Models;
using HelloWord.Library.Services;
using HelloWord.UniversalApp.Events;
using HelloWord.UniversalApp.Services;
using HelloWord.UniversalApp.Utils;
using HelloWord.UniversalApp.ViewModels.Base;
using HelloWord.UniversalApp.ViewModels.Controls;

namespace HelloWord.UniversalApp.ViewModels
{
    /// <summary>
    /// Home page's view model
    /// </summary>
    public class HomeViewModel : BaseViewModel
    {
        #region Attributes
        
        private ObservableCollection<Player> _players;
        private ObservableCollection<Player> _suggestedPlayers;
        private LanguagesViewModel _languagesViewModel;

        #endregion

        #region Properties

        /// <summary>
        /// List of all the players
        /// </summary>
        public ObservableCollection<Player> Players
        {
            get => _players;
            set
            {
                if (Equals(_players, value))
                {
                    return;
                }

                _players = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// List of the suggested players in the auto suggestion box
        /// </summary>
        public ObservableCollection<Player> SuggestedPlayers
        {
            get => _suggestedPlayers;
            set
            {
                if (Equals(_suggestedPlayers, value))
                {
                    return;
                }

                _suggestedPlayers = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Language control's view model
        /// </summary>
        public LanguagesViewModel LanguagesViewModel
        {
            get => _languagesViewModel;
            set
            {
                if (Equals(_languagesViewModel, value))
                {
                    return;
                }

                _languagesViewModel = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Commands
        
        /// <summary>
        /// Command executed when the page has loaded
        /// </summary>
        public ICommand PageLoadedCommand { get; set; }

        /// <summary>
        /// Command executed when an existing player is selected
        /// </summary>
        public ICommand PlayerSelectedCommand { get; set; }

        /// <summary>
        /// Command executed when the navigate button is clicked
        /// </summary>
        public ICommand NavigateToMenuCommand { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public HomeViewModel()
        {
            SuggestedPlayers = new ObservableCollection<Player>();
            LanguagesViewModel = new LanguagesViewModel();

            PageLoadedCommand = new RelayCommand<object>(async param => await PageLoadedAsync());
            PlayerSelectedCommand = new RelayCommand<AutoSuggestBoxSuggestionChosenEventArgs>(PlayerSelectedExecute);
            NavigateToMenuCommand = new RelayCommand<object>(param => NavigateToMenuExecute(), param => NavigateToMenuCanExecute());

            PlayerChanged += (sender, args) =>
            {
                if (Player.NativeLanguage != null)
                {
                    LanguagesViewModel.NativeLanguage = Player.NativeLanguage;
                }

                if (Player.FluentLanguage != null)
                {
                    LanguagesViewModel.FluentLanguage = Player.FluentLanguage;
                }
            };

            LanguagesViewModel.LanguagesLoaded += OnLanguagesLoaded;
            LanguagesViewModel.LanguageChanged += OnLanguageChanged;
        }

        #endregion

        #region Methods

        private async Task PageLoadedAsync()
        {
            Players = new ObservableCollection<Player>(
                await HttpClientService.Instance.GetAllAsync<Player>("/api/players"));
        }

        private void PlayerSelectedExecute(AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            Player = (Player) args.SelectedItem;
        }

        private bool NavigateToMenuCanExecute()
        {
            return Player != null && !string.IsNullOrEmpty(Player.Name);
        }

        private void OnLanguagesLoaded(object sender, EventArgs e)
        {
            Player = new Player
            {
                NativeLanguage = LanguagesViewModel.Languages[0],
                FluentLanguage = LanguagesViewModel.Languages[1]
            };
        }

        private void OnLanguageChanged(object sender, EventArgs e)
        {
            var eventArgs = (LanguageEventArgs) e;

            if (eventArgs.Type == "native")
            {
                Player.NativeLanguage = LanguagesViewModel.NativeLanguage;
            }
            else
            {
                Player.FluentLanguage = LanguagesViewModel.FluentLanguage;
            }
        }

        private async void NavigateToMenuExecute()
        {
            if (Player.Id == 0)
            {
                var random = new Random();
                var colors = Enum.GetValues(typeof(Color));
                Player.Color = (Color) colors.GetValue(random.Next(colors.Length));
                await HttpClientService.Instance.CreateAsync("/api/players", Player);
            }
            else
            {
                await HttpClientService.Instance.UpdateAsync(string.Format($"/api/players/{Player.Id}"), Player);
            }

            NavigationService.Instance.NavigateTo("Menu", Player);
        }

        #endregion
    }
}
