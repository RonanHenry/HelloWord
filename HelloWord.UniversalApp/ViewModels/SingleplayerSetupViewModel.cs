﻿using System;
using System.Linq;
using System.Windows.Input;
using HelloWord.UniversalApp.Events;
using HelloWord.UniversalApp.PresentationModels;
using HelloWord.UniversalApp.Services;
using HelloWord.UniversalApp.Utils;
using HelloWord.UniversalApp.ViewModels.Base;
using HelloWord.UniversalApp.ViewModels.Controls;

namespace HelloWord.UniversalApp.ViewModels
{
    /// <summary>
    /// Singleplayer setup view model
    /// </summary>
    public class SingleplayerSetupViewModel : BaseViewModel
    {
        #region Properties

        /// <summary>
        /// Game setup control's view model
        /// </summary>
        public GameSetupViewModel GameSetupViewModel { get; set; }

        /// <summary>
        /// Contains the game's settings
        /// </summary>
        public GameDataPresentation GameSettingsPresentation { get; set; }

        /// <summary>
        /// Check if the game can be launched
        /// </summary>
        public bool CanContinue { get; set; }

        #endregion

        #region Commands

        /// <summary>
        /// Command to launch the game
        /// </summary>
        public ICommand GameCommand { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public SingleplayerSetupViewModel()
        {
            GameSetupViewModel = new GameSetupViewModel();
            Title = ResourceLoader.GetString("SettingsText");

            GameCommand = new RelayCommand<object>(param => GameExecute(), param => GameCanExecute());

            GameSetupViewModel.DataChangesRequested += RefreshingGameSettings;
            GameSetupViewModel.DataLoaded += RefreshedGameSettings;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Register the player in the game setup control to initialize the data
        /// </summary>
        public void SynchronizePlayer()
        {
            GameSetupViewModel.Player = Player;
            GameSetupViewModel.InitControl();
        }

        private void RefreshingGameSettings(object sender, EventArgs e)
        {
            CanContinue = false;
            ((RelayCommand<object>) GameCommand).RaiseCanExecuteChanged();
        }

        private void RefreshedGameSettings(object sender, EventArgs e)
        {
            var args = (GameSettingsEventArgs) e;

            GameSettingsPresentation = new GameDataPresentation
            {
                Player = args.Player,
                Category = args.Category,
                Letter = args.Letter,
                CategoryWords = args.Words.Select(w => w.ToLower()).ToList()
            };

            CanContinue = true;
            ((RelayCommand<object>) GameCommand).RaiseCanExecuteChanged();
        }

        private bool GameCanExecute()
        {
            return CanContinue;
        }

        private void GameExecute()
        {
            NavigationService.Instance.NavigateTo("Game", GameSettingsPresentation);
        }

        #endregion
    }
}
