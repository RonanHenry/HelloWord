﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using HelloWord.Library.Enums;
using HelloWord.Library.Events;
using HelloWord.Library.Models;
using HelloWord.Library.Services;
using HelloWord.UniversalApp.PresentationModels;
using HelloWord.UniversalApp.Utils;
using HelloWord.UniversalApp.ViewModels.Base;
using HelloWord.UniversalApp.ViewModels.Controls;
using Language = Windows.Globalization.Language;

namespace HelloWord.UniversalApp.ViewModels
{
    /// <summary>
    /// Game view model
    /// </summary>
    public class GameViewModel : BaseViewModel
    {
        #region Attributes

        private bool _readyVisible;
        private bool _gameVisible;

        #endregion

        #region Properties

        /// <summary>
        /// Holds the game's data
        /// </summary>
        public GameDataPresentation GameData { get; set; }

        /// <summary>
        /// View model of the game header
        /// </summary>
        public GameHeaderViewModel GameHeaderViewModel { get; set; }

        /// <summary>
        /// Is the "Ready to play" message visible
        /// </summary>
        public bool ReadyVisible
        {
            get => _readyVisible;
            set
            {
                if (Equals(_readyVisible, value))
                {
                    return;
                }

                _readyVisible = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Is the game's interface visible
        /// </summary>
        public bool GameVisible
        {
            get => _gameVisible;
            set
            {
                if (Equals(_gameVisible, value))
                {
                    return;
                }

                _gameVisible = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Indicate if the game is over
        /// </summary>
        public bool GameDone { get; set; }

        /// <summary>
        /// Game's duration
        /// </summary>
        public int Seconds { get; set; }

        #endregion

        #region Commands

        /// <summary>
        /// Command to start the game
        /// </summary>
        public ICommand StartGameCommand { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public GameViewModel()
        {
            BackImage = "back-arrow-dark";
            HeaderBackground = (Brush) Application.Current.Resources["WhiteBrush"];
            TitleForeground = (Brush) Application.Current.Resources["DarkGreyBrush"];
            Title = ResourceLoader.GetString("GameText");
            ReadyVisible = true;
            GameVisible = false;
            GameDone = false;

            StartGameCommand = new RelayCommand<object>(async param => await StartGameExecute());
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initialize data on the game header control
        /// </summary>
        public void InitializeHeaderControl()
        {
            Player = GameData.Player;
            int seconds;

            switch (Player.Difficulty)
            {
                case Difficulty.Easy:
                    seconds = 60;
                    break;
                case Difficulty.Hard:
                    seconds = 15;
                    break;
                default:
                    seconds = 30;
                    break;
            }

            Seconds = seconds;

            GameHeaderViewModel = new GameHeaderViewModel
            {
                Player = Player,
                Category = GameData.Category,
                Letter = GameData.Letter,
                Timer = seconds
            };
        }

        private static async Task TimerDelay()
        {
            await Task.Delay(1000);
        }

        private async Task StartGameExecute()
        {
            ReadyVisible = false;
            GameVisible = true;

            var speechLanguage = 
                Player.FluentLanguage.Locale == "en" ? new Language("en-US") : new Language("fr-FR");

            SpeechToTextService.Instance.HasResult += SpeechResult;
            SpeechToTextService.Instance.StartRecognization(
                speechLanguage, GameData.CategoryWords.ToArray());

            while (GameHeaderViewModel.Timer > 0)
            {
                await TimerDelay();
                GameHeaderViewModel.Timer--;
            }

            SaveGameAsync();
        }

        private async void SpeechResult(object sender, SpeechToTextEventArgs e)
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
                CoreDispatcherPriority.Normal, () => ValidateWord(e.SpeechResult));
        }

        /// <summary>
        /// Check if the typed word is in the list
        /// </summary>
        /// <param name="word">Word to validate</param>
        public void TypingResult(string word)
        {
            if (GameData.CategoryWords.Any(w => w == word.ToLower()))
            {
                ValidateWord(word);
            }
        }

        private void ValidateWord(string word)
        {
            var firstLetter = word.ToUpper()[0];

            if (firstLetter == 'É')
            {
                firstLetter = 'E';
            }

            if (GameDone 
                || firstLetter != GameData.Letter
                || GameData.FoundWords.Any(
                    w => string.Equals(w.ToLower(), word.ToLower())))
            {
                return;
            }

            var fluentCulture = new CultureInfo(Player.FluentLanguage.Locale);

            GameData.FoundWords.Add(
                fluentCulture.TextInfo.ToTitleCase(word.Replace("\r", "")));
        }

        private async void SaveGameAsync()
        {
            SpeechToTextService.Instance.StopRecognization();
            GameDone = true;

            var dialog = new ContentDialog
            {
                Title = ResourceLoader.GetString("TimeUpText"),
                Content = string.Format(
                    ResourceLoader.GetString("GameRecapText"),
                        GameData.FoundWords.Count, Seconds),
                CloseButtonText = ResourceLoader.GetString("CloseText")
            };

            await dialog.ShowAsync();

            var game = new Game(Player.FluentLanguage, Player.Difficulty, new List<Player>
            {
                Player
            });

            game = await HttpClientService.Instance.CreateAsync("/api/games", game);

            var playerStat = await HttpClientService.Instance.GetSingleAsync<Stat>(
                string.Format($"/api/players/{Player.Id}/stats/{game.Id}"));

            playerStat.Score = GameData.FoundWords.Count;

            await HttpClientService.Instance.UpdateAsync(
                string.Format($"/api/stats/{playerStat.Id}"), playerStat);
        }

        #endregion
    }
}
