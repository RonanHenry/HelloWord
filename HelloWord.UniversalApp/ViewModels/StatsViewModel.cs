﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using HelloWord.Library.Enums;
using HelloWord.Library.Models;
using HelloWord.Library.Services;
using HelloWord.UniversalApp.PresentationModels;
using HelloWord.UniversalApp.Utils;
using HelloWord.UniversalApp.ViewModels.Base;

namespace HelloWord.UniversalApp.ViewModels
{
    /// <summary>
    /// Stats view model
    /// </summary>
    public class StatsViewModel : BaseViewModel
    {
        #region Attributes

        private ObservableCollection<StatPresentation> _stats;
        private bool _loadingStats;

        #endregion

        #region Properties

        /// <summary>
        /// Stats list formatted for display
        /// </summary>
        public ObservableCollection<StatPresentation> Stats
        {
            get => _stats;
            set
            {
                if (Equals(_stats, value))
                {
                    return;
                }

                _stats = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Is the request still going
        /// </summary>
        public bool LoadingStats
        {
            get => _loadingStats;
            set
            {
                if (Equals(_loadingStats, value))
                {
                    return;
                }

                _loadingStats = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Commands

        /// <summary>
        /// Page loaded command
        /// </summary>
        public ICommand PageLoadedCommand { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public StatsViewModel()
        {
            Title = ResourceLoader.GetString("StatsText");
            Stats = new ObservableCollection<StatPresentation>();
            LoadingStats = true;

            PageLoadedCommand = new RelayCommand<object>(async param => await PageLoadedAsync());
        }

        #endregion

        #region Methods

        private async Task PageLoadedAsync()
        {
            var languages = new List<Language>(
                await HttpClientService.Instance.GetAllAsync<Language>("/api/languages"));

            var playersStats = (await HttpClientService.Instance.GetAllAsync<Stat>("/api/stats"))
                .OrderByDescending(s => s.Player.Id == Player.Id)
                .ThenByDescending(s => s.Score)
                .GroupBy(s => s.Player.Id)
                .ToList();

            foreach (var playerStats in playersStats)
            {
                var languagesStats = playerStats
                    .GroupBy(s => s.Game.LanguageId)
                    .Select(s => s.ToList())
                    .ToList();

                var playerDisplayed = true;

                foreach (var languageStats in languagesStats)
                {
                    var player = languageStats[0].Player;

                    var language = languages
                        .FirstOrDefault(l => l.Id == languageStats[0].Game.LanguageId);

                    var easyScore = 0;
                    var mediumScore = 0;
                    var hardScore = 0;

                    foreach (var stat in languageStats)
                    {
                        switch (stat.Game.Difficulty)
                        {
                            case Difficulty.Easy:
                                easyScore += stat.Score;
                                break;
                            case Difficulty.Hard:
                                hardScore += stat.Score;
                                break;
                            default:
                                mediumScore += stat.Score;
                                break;
                        }
                    }

                    Stats.Add(new StatPresentation
                    {
                        Player = player,
                        Language = language,
                        EasyScore = easyScore,
                        MediumScore = mediumScore,
                        HardScore = hardScore,
                        PlayerDisplayed = playerDisplayed
                    });

                    playerDisplayed = false;
                }
            }

            LoadingStats = false;
        }

        #endregion
    }
}
