﻿using Windows.UI.Xaml.Controls;
using HelloWord.UniversalApp.Services;

namespace HelloWord.UniversalApp.ViewModels
{
    /// <summary>
    /// Main view model
    /// </summary>
    public class MainViewModel
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="frame">Frame wrapping the app</param>
        public MainViewModel(Frame frame)
        {
            NavigationService.Instance.NavigationFrame = frame;
            NavigationService.Instance.NavigateTo("Home");
        }

        #endregion
    }
}
