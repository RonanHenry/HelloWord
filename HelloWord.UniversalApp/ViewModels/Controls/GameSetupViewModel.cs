﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Input;
using HelloWord.Library.Enums;
using HelloWord.Library.Services;
using HelloWord.UniversalApp.Events;
using HelloWord.UniversalApp.Utils;
using HelloWord.UniversalApp.ViewModels.Base;
using Newtonsoft.Json.Linq;

namespace HelloWord.UniversalApp.ViewModels.Controls
{
    /// <summary>
    /// Game setup view model
    /// </summary>
    public class GameSetupViewModel : BaseViewModel
    {
        #region Attributes

        private string _category;
        private char _letter;
        private string _timer;
        private bool _loadingCategory;
        private bool _loadingLetter;

        #endregion

        #region Properties

        /// <summary>
        /// Categories list
        /// </summary>
        public List<string> Categories { get; set; }

        /// <summary>
        /// Letters list
        /// </summary>
        public List<char> Letters { get; set; }

        /// <summary>
        /// Words list
        /// </summary>
        public List<string> Words { get; set; }

        /// <summary>
        /// Selected category
        /// </summary>
        public string Category
        {
            get => _category;
            set
            {
                if (Equals(_category, value))
                {
                    return;
                }

                _category = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Selected letter
        /// </summary>
        public char Letter
        {
            get => _letter;
            set
            {
                if (Equals(_letter, value))
                {
                    return;
                }

                _letter = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Timer
        /// </summary>
        public string Timer
        {
            get => _timer;
            set
            {
                if (Equals(_timer, value))
                {
                    return;
                }

                _timer = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// State of the category loading
        /// </summary>
        public bool LoadingCategory
        {
            get => _loadingCategory;
            set
            {
                if (Equals(_loadingCategory, value))
                {
                    return;
                }

                _loadingCategory = value;
                OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// State of the letter loading
        /// </summary>
        public bool LoadingLetter
        {
            get => _loadingLetter;
            set
            {
                if (Equals(_loadingLetter, value))
                {
                    return;
                }

                _loadingLetter = value;
                OnPropertyChanged();
            }
        }

        private JArray Data { get; set; }

        private int CategoryId { get; set; }

        #endregion

        #region Commands

        /// <summary>
        /// Refresh the category and the letter
        /// </summary>
        public ICommand RefreshGameSetupCommand { get; set; }

        #endregion

        #region Events

        /// <summary>
        /// Event triggered when the data is asked to change
        /// </summary>
        public event EventHandler DataChangesRequested;

        /// <summary>
        /// Event triggered when the data is loaded
        /// </summary>
        public event EventHandler DataLoaded;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public GameSetupViewModel()
        {
            LoadingCategory = true;
            LoadingLetter = true;

            Categories = new List<string>();
            Letters = "ABCDEFGHJKLMNOPRSTV".ToList();
            Words = new List<string>();

            RefreshGameSetupCommand = new RelayCommand<object>(param =>
            {
                OnDataChangesRequested(EventArgs.Empty);
                LoadingCategory = true;
                LoadingLetter = true;

                RefreshGameSettings();
            });
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initialize the control with data
        /// </summary>
        public async void InitControl()
        {
            int seconds;

            switch (Player.Difficulty)
            {
                case Difficulty.Easy:
                    seconds = 60;
                    break;
                case Difficulty.Hard:
                    seconds = 15;
                    break;
                default:
                    seconds = 30;
                    break;
            }

            Timer = string.Format($"{seconds} {ResourceLoader.GetString("SecondsText")}");

            Data = JArray.Parse(
                await HttpClientService.Instance.GetStringAsync(
                    string.Format($"{HttpClientService.BaseUrl}/api/games/words")));

            foreach (var languageData in Data)
            {
                if (languageData["language"].ToString() != Player.NativeLanguage.Locale)
                {
                    continue;
                }

                foreach (var category in languageData["categories"])
                {
                    Categories.Add(
                        CultureInfo.CurrentCulture.TextInfo.ToTitleCase(
                            category["name"].ToString()));
                }
            }

            RefreshGameSettings();
        }

        private void RefreshGameSettings()
        {
            var random = new Random();

            var categoryRand = random.Next(Categories.Count);
            Category = Categories[categoryRand];
            CategoryId = categoryRand + 1;
            LoadingCategory = false;

            Letter = Letters[random.Next(Letters.Count)];
            LoadingLetter = false;

            Words.Clear();

            foreach (var languageData in Data)
            {
                if (languageData["language"].ToString() != Player.FluentLanguage.Locale)
                {
                    continue;
                }

                var fluentCulture = new CultureInfo(Player.FluentLanguage.Locale);

                foreach (var category in languageData["categories"])
                {
                    if ((int) category["id"] != CategoryId)
                    {
                        continue;
                    }

                    foreach (var word in category["words"])
                    {
                        Words.Add(fluentCulture.TextInfo.ToTitleCase(word.ToString()));
                    }
                }
            }

            OnDataLoaded(new GameSettingsEventArgs
            {
                Player = Player,
                Category = Category,
                Letter = Letter,
                Words = Words
            });
        }

        /// <summary>
        /// Trigger the data changes requested event
        /// </summary>
        /// <param name="e">Event arguments</param>
        public virtual void OnDataChangesRequested(EventArgs e)
        {
            var handler = DataChangesRequested;

            handler?.Invoke(this, e);
        }

        /// <summary>
        /// Trigger the data loaded event
        /// </summary>
        /// <param name="e">Game settings event arguments</param>
        public virtual void OnDataLoaded(GameSettingsEventArgs e)
        {
            var handler = DataLoaded;

            handler?.Invoke(this, e);
        }

        #endregion
    }
}
