﻿using HelloWord.Library.Models;
using HelloWord.UniversalApp.ViewModels.Base;

namespace HelloWord.UniversalApp.ViewModels.Controls
{
    /// <summary>
    /// Game header view model
    /// </summary>
    public class GameHeaderViewModel : BaseViewModel
    {
        #region Attributes

        private Player _player;
        private string _category;
        private char _letter;
        private int _timer;

        #endregion

        #region Properties

        /// <summary>
        /// Player
        /// </summary>
        public Player Player
        {
            get => _player;
            set
            {
                if (Equals(_player, value))
                {
                    return;
                }

                _player = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Category of words
        /// </summary>
        public string Category
        {
            get => _category;
            set
            {
                if (Equals(_category, value))
                {
                    return;
                }

                _category = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Letter
        /// </summary>
        public char Letter
        {
            get => _letter;
            set
            {
                if (Equals(_letter, value))
                {
                    return;
                }

                _letter = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Seconds remaining on the timer
        /// </summary>
        public int Timer
        {
            get => _timer;
            set
            {
                if (Equals(_timer, value))
                {
                    return;
                }

                _timer = value;
                OnPropertyChanged();
            }
        }

        #endregion
    }
}
