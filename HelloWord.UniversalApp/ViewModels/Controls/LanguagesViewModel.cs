﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Windows.UI.Xaml.Media;
using HelloWord.Library.Models;
using HelloWord.Library.Services;
using HelloWord.UniversalApp.Events;
using HelloWord.UniversalApp.Utils;
using HelloWord.UniversalApp.ViewModels.Base;

namespace HelloWord.UniversalApp.ViewModels.Controls
{
    /// <summary>
    /// Languages view model
    /// </summary>
    public class LanguagesViewModel : BaseViewModel
    {
        #region Attributes

        private ObservableCollection<Language> _languages;
        private Language _nativeLanguage;
        private Language _fluentLanguage;

        #endregion

        #region Properties

        /// <summary>
        /// List of languages
        /// </summary>
        public ObservableCollection<Language> Languages
        {
            get => _languages;
            set
            {
                if (Equals(_languages, value))
                {
                    return;
                }

                _languages = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Selected native language
        /// </summary>
        public Language NativeLanguage
        {
            get => _nativeLanguage;
            set
            {
                if (Equals(_nativeLanguage, value))
                {
                    return;
                }

                _nativeLanguage = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Selected fluent language
        /// </summary>
        public Language FluentLanguage
        {
            get => _fluentLanguage;
            set
            {
                if (Equals(_fluentLanguage, value))
                {
                    return;
                }

                _fluentLanguage = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Flag border color
        /// </summary>
        public Brush FlagBorderColor { get; set; }

        /// <summary>
        /// Flag foreground color
        /// </summary>
        public Brush FlagForegroundColor { get; set; }

        /// <summary>
        /// Which language is being changed
        /// </summary>
        public string ChangingLanguage { get; set; }

        #endregion

        #region Events

        /// <summary>
        /// Languages list loaded event
        /// </summary>
        public event EventHandler LanguagesLoaded;

        /// <summary>
        /// Selected language changed event
        /// </summary>
        public event EventHandler LanguageChanged;

        #endregion

        #region Commands

        /// <summary>
        /// Changing language command
        /// </summary>
        public ICommand ChangingLanguageCommand { get; set; }

        /// <summary>
        /// Language selected command
        /// </summary>
        public ICommand LanguageSelectedCommand { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public LanguagesViewModel()
        {
            ChangingLanguageCommand = new RelayCommand<object>(param =>
            {
                ChangingLanguage = param.ToString();
            });

            LanguageSelectedCommand = new RelayCommand<LanguageEventArgs>(LanguageSelectedCommandExecute);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Control loaded event handler method
        /// </summary>
        public async void ControlLoadedAsync()
        {
            Languages = new ObservableCollection<Language>(
                await HttpClientService.Instance.GetAllAsync<Language>("/api/languages"));

            OnLanguagesLoaded(EventArgs.Empty);
        }

        /// <summary>
        /// Language selected command handler method
        /// </summary>
        /// <param name="e">Language event arguments</param>
        private void LanguageSelectedCommandExecute(LanguageEventArgs e)
        {
            if (ChangingLanguage == "native")
            {
                NativeLanguage = Languages.FirstOrDefault(l => l.Id == e.Id);
            }
            else
            {
                FluentLanguage = Languages.FirstOrDefault(l => l.Id == e.Id);
            }

            OnLanguageChanged(new LanguageEventArgs
            {
                Id = e.Id,
                Type = ChangingLanguage
            });
        }

        /// <summary>
        /// Languages list loaded event handler method
        /// </summary>
        /// <param name="e">Event arguments</param>
        public virtual void OnLanguagesLoaded(EventArgs e)
        {
            var handler = LanguagesLoaded;

            handler?.Invoke(this, e);
        }

        /// <summary>
        /// Language selected event handler method
        /// </summary>
        /// <param name="e">Event arguments</param>
        public virtual void OnLanguageChanged(LanguageEventArgs e)
        {
            var handler = LanguageChanged;

            handler?.Invoke(this, e);
        }

        #endregion
    }
}
