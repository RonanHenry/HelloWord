﻿using System;
using System.Windows.Input;
using Windows.UI.Xaml.Controls;
using HelloWord.UniversalApp.Services;
using HelloWord.UniversalApp.Utils;
using HelloWord.UniversalApp.ViewModels.Base;

namespace HelloWord.UniversalApp.ViewModels
{
    /// <summary>
    /// Menu view model
    /// </summary>
    public class MenuViewModel : BaseViewModel
    {
        #region Commands

        /// <summary>
        /// Singleplayer command
        /// </summary>
        public ICommand SingleplayerCommand { get; set; }

        /// <summary>
        /// Multiplayer command
        /// </summary>
        public ICommand MultiplayerCommand { get; set; }

        /// <summary>
        /// Stats command
        /// </summary>
        public ICommand StatsCommand { get; set; }

        /// <summary>
        /// Settings command
        /// </summary>
        public ICommand SettingsCommand { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public MenuViewModel()
        {
            Title = ResourceLoader.GetString("BackText");

            SingleplayerCommand = new RelayCommand<object>(param => SingleplayerExecute());
            MultiplayerCommand = new RelayCommand<object>(param => MultiplayerExecute());
            StatsCommand = new RelayCommand<object>(param => StatsExecute());
            SettingsCommand = new RelayCommand<object>(param => SettingsExecute());
        }

        #endregion

        #region Methods

        private void SingleplayerExecute()
        {
            NavigationService.Instance.NavigateTo("SingleplayerSetup", Player);
        }

        private async void MultiplayerExecute()
        {
            var dialog = new ContentDialog
            {
                Title = ResourceLoader.GetString("ComingSoonTitle"),
                Content = ResourceLoader.GetString("ComingSoonText"),
                CloseButtonText = ResourceLoader.GetString("CloseText")
            };

            await dialog.ShowAsync();
        }

        private void StatsExecute()
        {
            NavigationService.Instance.NavigateTo("Stats", Player);
        }

        private void SettingsExecute()
        {
            NavigationService.Instance.NavigateTo("Settings", Player);
        }

        #endregion
    }
}
