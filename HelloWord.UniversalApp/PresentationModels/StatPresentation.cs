﻿using HelloWord.Library.Models;
using HelloWord.Library.Utils;

namespace HelloWord.UniversalApp.PresentationModels
{
    /// <summary>
    /// Stat view presentation model
    /// </summary>
    public class StatPresentation : PropertyChangedWatcher
    {
        #region Attributes

        private Player _player;
        private Language _language;
        private int _easyScore;
        private int _mediumScore;
        private int _hardScore;
        private bool _playerDisplayed;

        #endregion

        #region Properties

        /// <summary>
        /// Stat's player
        /// </summary>
        public Player Player
        {
            get => _player;
            set
            {
                if (Equals(_player, value))
                {
                    return;
                }

                _player = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Stat's language
        /// </summary>
        public Language Language
        {
            get => _language;
            set
            {
                if (Equals(_language, value))
                {
                    return;
                }

                _language = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Stat's easy score
        /// </summary>
        public int EasyScore
        {
            get => _easyScore;
            set
            {
                if (Equals(_easyScore, value))
                {
                    return;
                }

                _easyScore = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Stat's medium score
        /// </summary>
        public int MediumScore
        {
            get => _mediumScore;
            set
            {
                if (Equals(_mediumScore, value))
                {
                    return;
                }

                _mediumScore = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Stat's hard score
        /// </summary>
        public int HardScore
        {
            get => _hardScore;
            set
            {
                if (Equals(_hardScore, value))
                {
                    return;
                }

                _hardScore = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Should the player be displayed?
        /// </summary>
        public bool PlayerDisplayed
        {
            get => _playerDisplayed;
            set
            {
                if (Equals(_playerDisplayed, value))
                {
                    return;
                }

                _playerDisplayed = value;
                OnPropertyChanged();
            }
        }

        #endregion
    }
}
