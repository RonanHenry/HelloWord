﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using HelloWord.Library.Models;
using HelloWord.Library.Utils;

namespace HelloWord.UniversalApp.PresentationModels
{
    /// <summary>
    /// Game presentation model
    /// </summary>
    public class GameDataPresentation : PropertyChangedWatcher
    {
        #region Attributes

        private Player _player;
        private string _category;
        private char _letter;
        private List<string> _categoryWords;
        private ObservableCollection<string> _foundWords;

        #endregion

        #region Properties

        /// <summary>
        /// Player
        /// </summary>
        public Player Player
        {
            get => _player;
            set
            {
                if (Equals(_player, value))
                {
                    return;
                }

                _player = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Category of words to find
        /// </summary>
        public string Category
        {
            get => _category;
            set
            {
                if (Equals(_category, value))
                {
                    return;
                }

                _category = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Letter
        /// </summary>
        public Char Letter
        {
            get => _letter;
            set
            {
                if (Equals(_letter, value))
                {
                    return;
                }

                _letter = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// List of words from the category
        /// </summary>
        public List<string> CategoryWords
        {
            get => _categoryWords;
            set
            {
                if (Equals(_categoryWords, value))
                {
                    return;
                }

                _categoryWords = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// List of words found by the player
        /// </summary>
        public ObservableCollection<string> FoundWords
        {
            get => _foundWords;
            set
            {
                if (Equals(_foundWords, value))
                {
                    return;
                }

                _foundWords = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public GameDataPresentation()
        {
            FoundWords = new ObservableCollection<string>();
        }

        #endregion
    }
}
