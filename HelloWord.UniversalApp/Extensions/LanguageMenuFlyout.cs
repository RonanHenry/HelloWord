﻿using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using HelloWord.Library.Models;
using HelloWord.UniversalApp.Events;

namespace HelloWord.UniversalApp.Extensions
{
    /// <summary>
    /// Special language menu flyout
    /// </summary>
    public class LanguageMenuFlyout : MenuFlyout
    {
        #region Dependency properties

        /// <summary>
        /// List of available languages dependency property
        /// </summary>
        public static readonly DependencyProperty LanguagesProperty = DependencyProperty.Register(
            "Languages",
            typeof(ObservableCollection<Language>),
            typeof(LanguageMenuFlyout),
            new PropertyMetadata(new ObservableCollection<Language>(), (sender, e) =>
            {
                var menu = (LanguageMenuFlyout) sender;
                menu.Items?.Clear();

                foreach (var language in (ObservableCollection<Language>) e.NewValue)
                {
                    var menuItem = new MenuFlyoutItem
                    {
                        Tag = language.Id,
                        Text = language.Name
                    };

                    menu.Items?.Add(menuItem);
                }

                if (menu.Items == null)
                {
                    return;
                }

                foreach (var menuItem in menu.Items)
                {
                    var item = (MenuFlyoutItem) menuItem;
                    item.Click += (o, args) => menu.OnItemSelected(new LanguageEventArgs
                    {
                        Id = (int) item.Tag
                    });
                }
            }));

        #endregion

        #region Properties

        /// <summary>
        /// List of available languages
        /// </summary>
        public ObservableCollection<Language> Languages
        {
            get => (ObservableCollection<Language>) GetValue(LanguagesProperty);
            set => SetValue(LanguagesProperty, value);
        }

        #endregion

        #region Events

        /// <summary>
        /// Event fired when a language is selected
        /// </summary>
        public event EventHandler ItemSelected;

        #endregion

        #region Methods

        /// <summary>
        /// Method used to fire the event passing the language arguments
        /// </summary>
        /// <param name="e">Language event arguments</param>
        protected virtual void OnItemSelected(LanguageEventArgs e)
        {
            var handler = ItemSelected;

            handler?.Invoke(this, e);
        }

        #endregion
    }
}
