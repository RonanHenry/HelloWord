﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace HelloWord.UniversalApp.Extensions
{
    /// <summary>
    /// Special flag button
    /// </summary>
    public class FlagButton : Button
    {
        #region Dependency Properties

        /// <summary>
        /// Border color dependency property
        /// </summary>
        public static readonly DependencyProperty FlagBorderBrushProperty = DependencyProperty.Register(
            "FlagBorderBrush",
            typeof(Brush),
            typeof(FlagButton),
            new PropertyMetadata(Application.Current.Resources["WhiteBrush"]));

        /// <summary>
        /// Flag image dependency property
        /// </summary>
        public static readonly DependencyProperty FlagImageProperty = DependencyProperty.Register(
            "FlagImage",
            typeof(ImageSource),
            typeof(FlagButton),
            new PropertyMetadata(null));

        /// <summary>
        /// Foreground color dependency property
        /// </summary>
        public static readonly DependencyProperty FlagForegroundBrushProperty = DependencyProperty.Register(
            "FlagForegroundBrush",
            typeof(Brush),
            typeof(FlagButton),
            new PropertyMetadata(Application.Current.Resources["WhiteBrush"]));

        #endregion

        #region Properties

        /// <summary>
        /// Border color
        /// </summary>
        public Brush FlagBorderBrush
        {
            get => (Brush) GetValue(FlagBorderBrushProperty);
            set => SetValue(FlagBorderBrushProperty, value);
        }

        /// <summary>
        /// Flag image
        /// </summary>
        public ImageSource FlagImage
        {
            get => (ImageSource) GetValue(FlagImageProperty);
            set => SetValue(FlagImageProperty, value);
        }

        /// <summary>
        /// Foreground color
        /// </summary>
        public Brush FlagForegroundBrush
        {
            get => (Brush) GetValue(FlagForegroundBrushProperty);
            set => SetValue(FlagForegroundBrushProperty, value);
        }

        #endregion
    }
}
