﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using HelloWord.UniversalApp.ViewModels.Controls;

namespace HelloWord.UniversalApp.Controls
{
    /// <summary>
    /// Game setup control code behind
    /// </summary>
    public sealed partial class GameSetupControl : UserControl
    {
        #region Properties

        /// <summary>
        /// View model
        /// </summary>
        public GameSetupViewModel Vm { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public GameSetupControl()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Control loaded event handler method
        /// </summary>
        /// <param name="sender">Object triggering the event</param>
        /// <param name="e">Routed event arguments</param>
        private void GameSetupControl_OnLoaded(object sender, RoutedEventArgs e)
        {
            Vm = (GameSetupViewModel) DataContext;
        }

        #endregion
    }
}
