﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using HelloWord.UniversalApp.ViewModels.Controls;

namespace HelloWord.UniversalApp.Controls
{
    public sealed partial class GameHeaderControl : UserControl
    {
        public GameHeaderViewModel Vm { get; set; }

        public GameHeaderControl()
        {
            this.InitializeComponent();
        }

        private void GameHeaderControl_OnLoaded(object sender, RoutedEventArgs e)
        {
            Vm = (GameHeaderViewModel) DataContext;
        }
    }
}
