﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using HelloWord.UniversalApp.Services;

namespace HelloWord.UniversalApp.Controls
{
    public sealed partial class BackControl : UserControl
    {
        public BackControl()
        {
            this.InitializeComponent();
        }

        private void BackButton_OnClick(object sender, RoutedEventArgs e)
        {
            NavigationService.Instance.GoBack();
        }
    }
}
