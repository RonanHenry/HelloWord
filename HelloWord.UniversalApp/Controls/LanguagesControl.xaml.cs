﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using HelloWord.UniversalApp.ViewModels.Controls;

namespace HelloWord.UniversalApp.Controls
{
    /// <summary>
    /// User control to select languages
    /// </summary>
    public sealed partial class LanguagesControl : UserControl
    {
        #region Properties

        /// <summary>
        /// Control's view model
        /// </summary>
        public LanguagesViewModel Vm { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public LanguagesControl()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Set the data context after the control is loaded
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Load event arguments</param>
        private void LanguagesControl_OnLoaded(object sender, RoutedEventArgs e)
        {
            Vm = (LanguagesViewModel) DataContext;
            Vm.ControlLoadedAsync();
        }

        #endregion
    }
}
