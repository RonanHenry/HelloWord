﻿using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using HelloWord.Library.Models;
using HelloWord.UniversalApp.ViewModels;
using Microsoft.HockeyApp;

namespace HelloWord.UniversalApp.Pages
{
    /// <summary>
    /// Menu page code behind
    /// </summary>
    public sealed partial class MenuPage : Page
    {
        #region Properties

        /// <summary>
        /// View model
        /// </summary>
        public MenuViewModel Vm { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public MenuPage()
        {
            InitializeComponent();
            HockeyClient.Current.TrackPageView(Name);

            Vm = new MenuViewModel();
            DataContext = Vm;
        }

        #endregion

        #region Methods

        /// <summary>
        /// On navigation handler method
        /// </summary>
        /// <param name="e">Navigation event</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Vm.Player = (Player) e.Parameter;
        }

        #endregion
    }
}
