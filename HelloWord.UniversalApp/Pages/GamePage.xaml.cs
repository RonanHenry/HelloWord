﻿using Windows.System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using HelloWord.UniversalApp.ViewModels;
using Windows.UI.Xaml.Navigation;
using HelloWord.UniversalApp.PresentationModels;
using Microsoft.HockeyApp;

namespace HelloWord.UniversalApp.Pages
{
    /// <summary>
    /// Game page code behind
    /// </summary>
    public sealed partial class GamePage : Page
    {
        #region Properties

        /// <summary>
        /// View model
        /// </summary>
        public GameViewModel Vm { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public GamePage()
        {
            InitializeComponent();
            HockeyClient.Current.TrackPageView(Name);
            
            Vm = new GameViewModel();
            DataContext = Vm;
        }

        #endregion

        #region Methods

        /// <summary>
        /// On navigation event handler method
        /// </summary>
        /// <param name="e">Navigation event arguments</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Vm.GameData = (GameDataPresentation) e.Parameter;
            Vm.InitializeHeaderControl();
        }

        #endregion

        private void WordTextbox_OnKeyDown(object sender, KeyRoutedEventArgs e)
        {
            var textbox = (TextBox) sender;

            if (e.Key != VirtualKey.Enter)
            {
                return;
            }

            Vm.TypingResult(textbox.Text);
            textbox.Text = "";
        }
    }
}
