﻿using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using HelloWord.Library.Models;
using HelloWord.UniversalApp.ViewModels;
using Microsoft.HockeyApp;

namespace HelloWord.UniversalApp.Pages
{
   /// <summary>
   /// Settings page code behind
   /// </summary>
    public sealed partial class SettingsPage : Page
    {
        #region Properties

        /// <summary>
        /// View model
        /// </summary>
        public SettingsViewModel Vm { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public SettingsPage()
        {
            InitializeComponent();
            HockeyClient.Current.TrackPageView(Name);

            Vm = new SettingsViewModel();
            DataContext = Vm;
        }

        #endregion

        #region Methods

        /// <summary>
        /// On navigation handler method
        /// </summary>
        /// <param name="e">Navigation event</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Vm.Player = (Player) e.Parameter;
            InitDifficultyRadioButtons();
        }

        /// <summary>
        /// Initialize radio buttons selection
        /// </summary>
        private void InitDifficultyRadioButtons()
        {
            foreach (var child in DifficultyStackPanel.Children)
            {
                if (child is RadioButton radio)
                {
                    radio.IsChecked = radio.CommandParameter as string == Vm.Player.Difficulty.ToString();
                }
            }
        }

        #endregion
    }
}
