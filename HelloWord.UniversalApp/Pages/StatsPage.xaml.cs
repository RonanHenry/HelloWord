﻿using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using HelloWord.Library.Models;
using HelloWord.UniversalApp.ViewModels;
using Microsoft.HockeyApp;

namespace HelloWord.UniversalApp.Pages
{
    /// <summary>
    /// Stats page code behind
    /// </summary>
    public sealed partial class StatsPage : Page
    {
        #region Properties

        /// <summary>
        /// View model
        /// </summary>
        public StatsViewModel Vm { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public StatsPage()
        {
            InitializeComponent();
            HockeyClient.Current.TrackPageView(Name);

            Vm = new StatsViewModel();
            DataContext = Vm;
        }

        #endregion

        #region Methods

        /// <summary>
        /// On navigation handler method
        /// </summary>
        /// <param name="e">Navigation event</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Vm.Player = (Player) e.Parameter;
        }

        #endregion
    }
}
