﻿using Windows.UI.Xaml.Controls;
using HelloWord.UniversalApp.ViewModels;
using Windows.UI.Xaml.Navigation;
using HelloWord.Library.Models;
using Microsoft.HockeyApp;

namespace HelloWord.UniversalApp.Pages
{
    /// <summary>
    /// Singleplayer setup page code behind
    /// </summary>
    public sealed partial class SingleplayerSetupPage : Page
    {
        #region Properties

        /// <summary>
        /// View model
        /// </summary>
        public SingleplayerSetupViewModel Vm { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public SingleplayerSetupPage()
        {
            InitializeComponent();
            HockeyClient.Current.TrackPageView(Name);

            Vm = new SingleplayerSetupViewModel();
            DataContext = Vm;
        }

        #endregion

        #region Methods

        /// <summary>
        /// On navigation event handler method
        /// </summary>
        /// <param name="e">Navigation event arguments</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Vm.Player = (Player) e.Parameter;
            Vm.SynchronizePlayer();
        }

        #endregion
    }
}
