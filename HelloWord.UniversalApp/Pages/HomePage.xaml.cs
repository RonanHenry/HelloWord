﻿using System;
using System.Linq;
using Windows.UI.Xaml.Controls;
using HelloWord.Library.Models;
using HelloWord.UniversalApp.Utils;
using HelloWord.UniversalApp.ViewModels;
using Microsoft.HockeyApp;

namespace HelloWord.UniversalApp.Pages
{
    /// <summary>
    /// Home page
    /// </summary>
    public sealed partial class HomePage : Page
    {
        #region Properties

        /// <summary>
        /// Home page view model
        /// </summary>
        public HomeViewModel Vm { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public HomePage()
        {
            InitializeComponent();
            HockeyClient.Current.TrackPageView(Name);

            Vm = new HomeViewModel();
            DataContext = Vm;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Event handler for the input change in the auto suggestion box
        /// </summary>
        /// <param name="sender">The auto suggestion box</param>
        /// <param name="args">Text changed event arguments</param>
        private void PlayerNameField_OnTextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (args.Reason != AutoSuggestionBoxTextChangeReason.UserInput 
                || Vm.Players == null
                || Vm.LanguagesViewModel.Languages == null
                || Vm.LanguagesViewModel.Languages.Count == 0)
            {
                return;
            }
            
            Vm.Player = new Player
            {
                Name = sender.Text,
                NativeLanguage = Vm.LanguagesViewModel.NativeLanguage,
                FluentLanguage = Vm.LanguagesViewModel.FluentLanguage
            };
            
            ((RelayCommand<object>) Vm.NavigateToMenuCommand).RaiseCanExecuteChanged();

            if (string.IsNullOrEmpty(sender.Text))
            {
                Vm.SuggestedPlayers.Clear();
                return;
            }

            var suggestions = Vm.Players
                .Where(p => p.Name.StartsWith(sender.Text, StringComparison.InvariantCultureIgnoreCase)).ToList();
            Vm.SuggestedPlayers.Clear();

            foreach (var player in suggestions)
            {
                Vm.SuggestedPlayers.Add(player);
            }
        }

        #endregion
    }
}
