﻿using System;
using Windows.UI.Xaml.Data;

namespace HelloWord.UniversalApp.Converters
{
    /// <summary>
    /// String to image path converter
    /// </summary>
    public class ImagePathConverter : IValueConverter
    {
        /// <summary>
        /// Convert a string to an image path
        /// </summary>
        /// <param name="value">The value to convert</param>
        /// <param name="targetType">Target type</param>
        /// <param name="parameter">Parameter</param>
        /// <param name="language">Language</param>
        /// <returns>The converted value</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var s = value as string;

            return string.IsNullOrEmpty(s) ? value : string.Format($"/Assets/Images/{s}.png");
        }

        /// <summary>
        /// Convert back to the original string value
        /// </summary>
        /// <param name="value">The value to convert back</param>
        /// <param name="targetType">Target type</param>
        /// <param name="parameter">Parameter</param>
        /// <param name="language">Language</param>
        /// <returns>The original value</returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
