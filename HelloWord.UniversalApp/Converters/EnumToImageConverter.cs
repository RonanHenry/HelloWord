﻿using System;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;

namespace HelloWord.UniversalApp.Converters
{
    /// <summary>
    /// Convert an enum value to an image path
    /// </summary>
    public class EnumToImageConverter : IValueConverter
    {
        /// <summary>
        /// Steps to perform the conversion
        /// </summary>
        /// <param name="value">Value to convert</param>
        /// <param name="targetType">Target type</param>
        /// <param name="parameter">Parameter</param>
        /// <param name="language">Language</param>
        /// <returns>The image path</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return new BitmapImage(new Uri(string.Format($"ms-appx:///Assets/Images/{value.ToString().ToLower()}.png")));
        }

        /// <summary>
        /// Steps to convert back to the original value
        /// </summary>
        /// <param name="value">Value</param>
        /// <param name="targetType">Target type</param>
        /// <param name="parameter">Parameter</param>
        /// <param name="language">Language</param>
        /// <returns>The original value</returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
