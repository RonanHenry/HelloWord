﻿using System;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;

namespace HelloWord.UniversalApp.Converters
{
    /// <summary>
    /// Convert a language locale into the corresponding flag image path
    /// </summary>
    public class FlagImageConverter : IValueConverter
    {
        /// <summary>
        /// Steps to perform the conversion
        /// </summary>
        /// <param name="value">Value to convert</param>
        /// <param name="targetType">Target type</param>
        /// <param name="parameter">Parameter</param>
        /// <param name="language">Language</param>
        /// <returns>The flag image path</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var s = value as string;
            var path = string.IsNullOrEmpty(s) ? s : string.Format($"/Assets/Images/flag-{s}.png");

            if (parameter != null && parameter as string == "image")
            {
                return new BitmapImage(new Uri(string.Format($"ms-appx:{path}"), UriKind.RelativeOrAbsolute));
            }

            return path;
        }

        /// <summary>
        /// Steps to convert back to the original value
        /// </summary>
        /// <param name="value">Value to convert</param>
        /// <param name="targetType">Target type</param>
        /// <param name="parameter">Parameter</param>
        /// <param name="language">Language</param>
        /// <returns>The original value</returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
