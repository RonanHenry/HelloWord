﻿using System;
using System.Collections.Generic;
using HelloWord.Library.Models;

namespace HelloWord.UniversalApp.Events
{
    /// <summary>
    /// Arguments associated with a game's settings being changed
    /// </summary>
    public class GameSettingsEventArgs : EventArgs
    {
        /// <summary>
        /// Player
        /// </summary>
        public Player Player { get; set; }

        /// <summary>
        /// Words catrgory
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Letter to start with
        /// </summary>
        public char Letter { get; set; }

        /// <summary>
        /// Words list
        /// </summary>
        public List<string> Words { get; set; }
    }
}
