﻿using System;

namespace HelloWord.UniversalApp.Events
{
    /// <summary>
    /// Arguments associated with a language changed event
    /// </summary>
    public class LanguageEventArgs : EventArgs
    {
        #region Properties

        /// <summary>
        /// Id of the selected language
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Type of the selected language
        /// </summary>
        public string Type { get; set; }

        #endregion
    }
}
